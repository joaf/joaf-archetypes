package net.joaf.archetype.app.appbasic.webui.conf;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author Author
 * @since 1.0
 */
public class SamplenameI18nPropertyDefinitionTest {

	@Test
	public void testPropertyFiles() throws Exception {
		//given
		//when
		List<String> strings = new SamplenameI18nPropertyDefinition().propertyFiles();
		//then
		assertEquals(1, strings.size());
	}
}