package net.joaf.archetype.app.appbasic.webui.controllers;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * @author Author
 * @since 1.0
 */
public class IndexControllerTest {

	@Test
	public void testIndexRedirect() throws Exception {
		//given
		//when
		String index = new IndexController().index();
		//then
		assertTrue(index.contains("redirect:"));
	}
}