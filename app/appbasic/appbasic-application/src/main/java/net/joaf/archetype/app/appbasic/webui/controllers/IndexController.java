package net.joaf.archetype.app.appbasic.webui.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Default index
 *
 * @author Author
 * @since 1.0
 */
@Controller
public class IndexController {
	@RequestMapping({"/","/index"})
	public String index() {
		return "redirect:/dashboard.html";
	}
}
