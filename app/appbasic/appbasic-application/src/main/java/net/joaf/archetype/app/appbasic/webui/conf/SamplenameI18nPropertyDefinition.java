package net.joaf.archetype.app.appbasic.webui.conf;

import net.joaf.base.language.utils.I18nPropertyDefinition;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

/**
 * Load language definitions
 *
 * @author Author
 * @since 1.0
 */
@Component
public class SamplenameI18nPropertyDefinition implements I18nPropertyDefinition {
    @Override
    public List<String> propertyFiles() {
        return Collections.singletonList("classpath:i18n/samplename");
    }
}
