CREATE DATABASE local_appbasic CHARACTER SET utf8 COLLATE utf8_general_ci;
GRANT ALL ON local_appbasic.* TO user_appbasic IDENTIFIED BY 'appbasicpasword';
FLUSH PRIVILEGES;