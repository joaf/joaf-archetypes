package net.joaf.archetypebase.extension.repository.mysql;

import net.joaf.archetypebase.extension.model.Samplename;
import net.joaf.archetypebase.extension.repository.SamplenameRepository;
import net.joaf.base.core.db.EObjectState;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.utils.DateUtils;
import net.joaf.base.db.mysql.AbstractRepositoryMysql;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 * TODO: description
 *
 * @author Author
 * @since 1.0
 */
@Repository
public class SamplenameRepositoryMysql extends AbstractRepositoryMysql<Samplename> implements SamplenameRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    protected String getTableName() {
        return "tablename";
    }

    @Override
    protected JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    @Override
    protected RowMapper<Samplename> getRowMapper() {
        return (rs, rowNum) -> {
            Samplename element = new Samplename();
            element.setUid(rs.getString("uid"));
            element.setCreated(DateUtils.dateToLocalDateTime(rs.getDate("created")));
            element.setUpdated(DateUtils.dateToLocalDateTime(rs.getDate("updated")));
            element.setObjectState(EObjectState.valueOf(rs.getString("objectState")));
            element.setSubjectUid(rs.getString("subjectUid"));
            return element;
        };
    }

    @Override
    public void insert(Samplename element) throws JoafDatabaseException {
        try {
            String sql = "INSERT INTO " + getTableName() + " (uid, created, updated, objectState, subjectUid)"
                    + "VALUES (?,?,?,?,?)";
            getJdbcTemplate().update(sql, element.getUid(), DateUtils.localDateTimeToDate(element.getCreated()), DateUtils.localDateTimeToDate(element.getUpdated()),
                    element.getObjectState().toString(), element.getSubjectUid());
        } catch (Exception e) {
            throw new JoafDatabaseException("Insert command error", e);
        }
    }

    @Override
    public void store(Samplename element) throws JoafDatabaseException {
        try {
            String sql = "UPDATE " + getTableName() + " SET updated=?, objectState=? " +
                    "WHERE uid=?";
            getJdbcTemplate().update(sql, DateUtils.localDateTimeToDate(element.getUpdated()),
                    element.getObjectState().toString(), element.getUid());
        } catch (Exception e) {
            throw new JoafDatabaseException("Update command error", e);
        }
    }

    @Override
    public Class<Samplename> getEntityClass() {
        return Samplename.class;
    }
}
