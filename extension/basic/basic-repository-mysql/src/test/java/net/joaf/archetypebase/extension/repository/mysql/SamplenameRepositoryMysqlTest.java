package net.joaf.archetypebase.extension.repository.mysql;

import net.joaf.archetypebase.extension.model.Samplename;
import net.joaf.base.core.db.EObjectState;
import net.joaf.base.testutils.db.mysql.InsertAttributesMatcher;
import net.joaf.base.testutils.db.mysql.UpdateAttributesMatcher;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author Author
 * @since 1.0
 */
public class SamplenameRepositoryMysqlTest {

    @Mock
    private JdbcTemplate jdbcTemplateMock;

    @InjectMocks
    private SamplenameRepositoryMysql objectUnderTest = new SamplenameRepositoryMysql();

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetTableName() throws Exception {
        //given
        //when
        String tableName = objectUnderTest.getTableName();
        //then
        assertEquals("tablename", tableName);
    }

    @Test
    public void testGetJdbcTemplate() throws Exception {
        //given
        //when
        JdbcTemplate jdbcTemplate = objectUnderTest.getJdbcTemplate();
        //then
        assertEquals(jdbcTemplateMock, jdbcTemplate);
    }

    @Test
    public void testGetRowMapper() throws Exception {
        //given
        //when
        RowMapper<Samplename> rowMapper = objectUnderTest.getRowMapper();
        //then
        assertNotNull(rowMapper);
    }

    @Test
    public void testInsert() throws Exception {
        //given
        Samplename element = new Samplename();
        element.setObjectState(EObjectState.NEW);
        //when
        objectUnderTest.insert(element);
        //then
        InsertAttributesMatcher.validInsertQuery(jdbcTemplateMock);
    }

    @Test
    public void testStore() throws Exception {
        //given
        Samplename element = new Samplename();
        element.setObjectState(EObjectState.ACCEPTED);
        //when
        objectUnderTest.store(element);
        //then
        UpdateAttributesMatcher.validUpdateQuery(jdbcTemplateMock);
    }

    @Test
    public void testGetEntityClass() throws Exception {
        //given
        //when
        Class<Samplename> entityClass = objectUnderTest.getEntityClass();
        //then
        assertEquals(Samplename.class, entityClass);
    }

}