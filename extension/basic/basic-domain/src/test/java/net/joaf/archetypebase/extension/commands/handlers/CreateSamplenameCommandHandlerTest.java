package net.joaf.archetypebase.extension.commands.handlers;

import net.joaf.archetypebase.extension.repository.SamplenameRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

/**
 * TODO: description
 *
 * @author Author
 * @since 1.0
 */
public class CreateSamplenameCommandHandlerTest {

    @Mock
    private SamplenameRepository repositoryMock;

    @InjectMocks
    private CreateSamplenameCommandHandler objectUnderTest;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testValidate() throws Exception {
        //given
        //when
        //then
    }

    @Test
    public void testExecute() throws Exception {
        //given
        //when
        //then
    }
}