package net.joaf.archetypebase.extension.queries;

import net.joaf.archetypebase.extension.model.Samplename;
import net.joaf.archetypebase.extension.repository.SamplenameRepository;
import net.joaf.base.core.cqrs.PageredRequest;
import net.joaf.base.core.cqrs.PageredResult;
import net.joaf.base.core.error.JoafDatabaseException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;

/**
 * TODO: description
 *
 * @author Author
 * @since 1.0
 */
public class SamplenameQueryTest {

    @Mock
    private SamplenameRepository repositoryMock;

    @InjectMocks
    private SamplenameQuery query = new SamplenameQuery();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void queryTest() throws JoafDatabaseException {
        //given
        PageredRequest request = new PageredRequest(Optional.<Integer>empty(), Optional.<Integer>empty());
        given(repositoryMock.findAll(request)).willReturn(new PageredResult<>(Collections.emptyList(), Optional.<Integer>empty(), Optional.<Integer>empty(), Optional.<Integer>empty()));
        //when
        PageredResult<Samplename> response = query.findAll(request);
        //then
        assertEquals(0, response.getResult().size());
    }
}
