package net.joaf.archetypebase.extension.commands.handlers;

import net.joaf.archetypebase.extension.commands.UndoTrashSamplenameCommand;
import net.joaf.archetypebase.extension.repository.SamplenameRepository;
import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.db.EObjectState;
import net.joaf.base.core.error.JoafException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

/**
 * TODO: description
 *
 * @author Author
 * @since 1.0
 */
@CommandHandlerComponent
public class UndoTrashSamplenameCommandHandler implements CommandHandler<UndoTrashSamplenameCommand, Object> {

    @Autowired(required = false)
    private SamplenameRepository repository;

    @Override
    public BindingResult validate(BindingResult originalBindingResult, UndoTrashSamplenameCommand command) throws JoafException {
        return originalBindingResult;
    }

    @Override
    public Object execute(UndoTrashSamplenameCommand command) throws JoafException {
        repository.updateObjectState(command.getElementUid(), EObjectState.ACCEPTED);
        return null;
    }
}