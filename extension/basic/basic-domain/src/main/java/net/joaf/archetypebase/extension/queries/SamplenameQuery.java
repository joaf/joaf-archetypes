package net.joaf.archetypebase.extension.queries;

import net.joaf.archetypebase.extension.model.Samplename;
import net.joaf.archetypebase.extension.repository.SamplenameRepository;
import net.joaf.base.core.cqrs.PageredRequest;
import net.joaf.base.core.cqrs.PageredResult;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.model.enums.EStandardAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * TODO: description
 *
 * @author Author
 * @since 1.0
 */
@Component
public class SamplenameQuery {

    @Autowired(required = false)
    private SamplenameRepository repository;

    public PageredResult<Samplename> findAll(PageredRequest pageredRequest) throws JoafDatabaseException {
        PageredResult<Samplename> result = repository.findAll(pageredRequest);
        result.getResult().stream().forEach(x -> {
            x.getActions().add(EStandardAction.EDIT);
            x.getActions().add(EStandardAction.TRASH);
        });
        return result;
    }

    public Samplename findOne(String id) throws JoafDatabaseException {
        return repository.findOne(id);
    }
}
