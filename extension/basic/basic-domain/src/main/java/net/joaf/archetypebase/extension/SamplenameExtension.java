package net.joaf.archetypebase.extension;

import net.joaf.base.extension.AbstractExtension;
import net.joaf.base.extension.JoafExtension;
import org.springframework.stereotype.Component;

/**
 * TODO: description
 *
 * @author Author
 * @since 1.0
 */
@Component
public class SamplenameExtension extends AbstractExtension implements JoafExtension {
    public static final String EXTENSION_BASE_PATH = "/samplename";
    public static final String EXTENSION_ADMIN_BASE_PATH = "/administration/samplename";

    @Override
    public String getExtensionMetadataFile() {
        return "/extension-samplename.xml";
    }
}