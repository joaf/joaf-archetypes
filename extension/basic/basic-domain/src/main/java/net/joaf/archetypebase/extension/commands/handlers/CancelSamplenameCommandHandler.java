package net.joaf.archetypebase.extension.commands.handlers;

import net.joaf.archetypebase.extension.commands.CancelSamplenameCommand;
import net.joaf.archetypebase.extension.model.Samplename;
import net.joaf.archetypebase.extension.repository.SamplenameRepository;
import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.db.EObjectState;
import net.joaf.base.core.error.JoafException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

/**
 * TODO: description
 *
 * @author Author
 * @since 1.0
 */
@CommandHandlerComponent
public class CancelSamplenameCommandHandler implements CommandHandler<CancelSamplenameCommand, Object> {

    @Autowired(required = false)
    private SamplenameRepository repository;

    @Override
    public BindingResult validate(BindingResult originalBindingResult, CancelSamplenameCommand command) throws JoafException {
        return originalBindingResult;
    }

    @Override
    public Object execute(CancelSamplenameCommand command) throws JoafException {
        Samplename element = repository.findOne(command.getElementUid());
        if (EObjectState.NEW.equals(element.getObjectState())) {
            repository.remove(command.getElementUid());
        }
        return null;
    }
}