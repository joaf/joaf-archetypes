package net.joaf.archetypebase.extension.commands.handlers;

import net.joaf.archetypebase.extension.commands.CreateSamplenameCommand;
import net.joaf.archetypebase.extension.model.Samplename;
import net.joaf.archetypebase.extension.repository.SamplenameRepository;
import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.db.EObjectState;
import net.joaf.base.core.error.JoafException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

import java.time.LocalDateTime;

/**
 * TODO: description
 *
 * @author Author
 * @since 1.0
 */
@CommandHandlerComponent
public class CreateSamplenameCommandHandler implements CommandHandler<CreateSamplenameCommand, Object> {

    @Autowired(required = false)
    private SamplenameRepository repository;

    @Override
    public BindingResult validate(BindingResult originalBindingResult, CreateSamplenameCommand command) throws JoafException {
        return originalBindingResult;
    }

    @Override
    public Object execute(CreateSamplenameCommand command) throws JoafException {
        Samplename element = new Samplename();
        element.setUid(repository.prepareId());
        element.setObjectState(EObjectState.NEW);
        LocalDateTime now = LocalDateTime.now();
        element.setCreated(now);
        element.setUpdated(now);
        element.setSubjectUid(command.getSubjectUid());
        repository.insert(element);
        return element;
    }
}