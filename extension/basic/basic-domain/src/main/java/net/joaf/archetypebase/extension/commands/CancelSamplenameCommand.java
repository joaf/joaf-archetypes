package net.joaf.archetypebase.extension.commands;

import net.joaf.archetypebase.extension.commands.handlers.CancelSamplenameCommandHandler;
import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractUidSuidCommand;

/**
 * TODO: description
 *
 * @author Author
 * @since 1.0
 */
@CommandDto(handlerClass = CancelSamplenameCommandHandler.class)
public class CancelSamplenameCommand extends AbstractUidSuidCommand implements Command {
    public CancelSamplenameCommand(String elementUid, String userUid, String subjectUid) {
        super(elementUid, userUid, subjectUid);
    }
}