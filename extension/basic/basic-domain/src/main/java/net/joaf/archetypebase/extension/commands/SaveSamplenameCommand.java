package net.joaf.archetypebase.extension.commands;

import net.joaf.archetypebase.extension.commands.handlers.SaveSamplenameCommandHandler;
import net.joaf.archetypebase.extension.model.Samplename;
import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractEntityCommand;

/**
 * TODO: description
 *
 * @author Author
 * @since 1.0
 */
@CommandDto(handlerClass = SaveSamplenameCommandHandler.class)
public class SaveSamplenameCommand extends AbstractEntityCommand<Samplename> implements Command {
    public SaveSamplenameCommand(Samplename element, String userUid) {
        super(element, userUid);
    }
}