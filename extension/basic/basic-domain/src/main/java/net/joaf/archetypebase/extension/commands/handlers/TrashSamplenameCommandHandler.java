package net.joaf.archetypebase.extension.commands.handlers;

import net.joaf.archetypebase.extension.commands.TrashSamplenameCommand;
import net.joaf.archetypebase.extension.repository.SamplenameRepository;
import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.db.EObjectState;
import net.joaf.base.core.error.JoafException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

/**
 * TODO: description
 *
 * @author Author
 * @since 1.0
 */
@CommandHandlerComponent
public class TrashSamplenameCommandHandler implements CommandHandler<TrashSamplenameCommand, Object> {

    @Autowired(required = false)
    private SamplenameRepository repository;

    @Override
    public BindingResult validate(BindingResult originalBindingResult, TrashSamplenameCommand command) throws JoafException {
        return originalBindingResult;
    }

    @Override
    public Object execute(TrashSamplenameCommand command) throws JoafException {
        repository.updateObjectState(command.getElementUid(), EObjectState.TRASH);
        return null;
    }
}