package net.joaf.archetypebase.extension.commands.handlers;

import net.joaf.archetypebase.extension.commands.SaveSamplenameCommand;
import net.joaf.archetypebase.extension.model.Samplename;
import net.joaf.archetypebase.extension.repository.SamplenameRepository;
import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.db.EObjectState;
import net.joaf.base.core.error.JoafException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

import java.time.LocalDateTime;

/**
 * TODO: description
 *
 * @author Author
 * @since 1.0
 */
@CommandHandlerComponent
public class SaveSamplenameCommandHandler implements CommandHandler<SaveSamplenameCommand, Object> {

    @Autowired(required = false)
    private SamplenameRepository repository;

    @Override
    public BindingResult validate(BindingResult originalBindingResult, SaveSamplenameCommand command) throws JoafException {
        return originalBindingResult;
    }

    @Override
    public Object execute(SaveSamplenameCommand command) throws JoafException {
        Samplename dbObject = repository.findOne(command.getElement().getUid());
        if (EObjectState.NEW.equals(dbObject.getObjectState())) {
            repository.updateObjectState(command.getElement().getUid(), EObjectState.ACCEPTED);
        }
        Samplename element = command.getElement();
        element.setUpdated(LocalDateTime.now());
        repository.store(element);
        return null;
    }
}