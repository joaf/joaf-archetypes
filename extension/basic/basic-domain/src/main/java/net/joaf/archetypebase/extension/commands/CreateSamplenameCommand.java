package net.joaf.archetypebase.extension.commands;

import net.joaf.archetypebase.extension.commands.handlers.CreateSamplenameCommandHandler;
import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractUidSuidCommand;

/**
 * TODO: description
 *
 * @author Author
 * @since 1.0
 */
@CommandDto(handlerClass = CreateSamplenameCommandHandler.class)
public class CreateSamplenameCommand extends AbstractUidSuidCommand implements Command {
    public CreateSamplenameCommand(String userUid, String subjectUid) {
        super(null, userUid, subjectUid);
    }
}