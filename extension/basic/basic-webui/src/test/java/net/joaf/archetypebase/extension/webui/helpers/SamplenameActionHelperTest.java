package net.joaf.archetypebase.extension.webui.helpers;

import net.joaf.base.core.model.enums.EStandardAction;
import net.joaf.base.core.web.model.WebActionDetails;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * TODO: description
 *
 * @author Author
 * @since 1.0
 */
public class SamplenameActionHelperTest {

	@Test
	public void testCreateActionDetails() throws Exception {
		//given
		EStandardAction action = EStandardAction.EDIT;
		//when
		WebActionDetails actionDetails = SamplenameActionHelper.createActionDetails(action, "123", "/xyz");
		//then
		assertEquals("/xyz/edit/123.html", actionDetails.getActionUrl());
		assertEquals(Boolean.FALSE, actionDetails.getRequireConfirmation());
	}
}