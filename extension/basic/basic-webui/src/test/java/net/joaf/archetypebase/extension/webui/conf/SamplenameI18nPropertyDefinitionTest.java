package net.joaf.archetypebase.extension.webui.conf;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author Author
 * @since 1.0
 */
public class SamplenameI18nPropertyDefinitionTest {

	private SamplenameI18nPropertyDefinition objectUnderTest = new SamplenameI18nPropertyDefinition();

	@Test
	public void testPropertyFiles() throws Exception {
		//given
		//when
		List<String> list = objectUnderTest.propertyFiles();
		//then
		assertNotNull(list);
		assertEquals(1, list.size());
	}
}