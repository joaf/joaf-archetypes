package net.joaf.archetypebase.extension.webui.model;

import junit.framework.TestCase;
import net.joaf.archetypebase.extension.model.Samplename;

/**
 * TODO: description
 *
 * @author Author
 * @since 1.0
 */
public class SamplenameFormTest extends TestCase {

	public void testToSamplename() throws Exception {
		//given
		SamplenameForm objectUnderTest = new SamplenameForm();
		objectUnderTest.setUid("123");
		//when
		Samplename element = objectUnderTest.toSamplename();
		//then
		assertEquals("123", element.getUid());
	}

	public void testConstruct() throws Exception {
		//given
		Samplename element = new Samplename();
		element.setUid("123");
		//when
		SamplenameForm objectUnderTest = new SamplenameForm(element);
		//then
		assertEquals("123", objectUnderTest.getUid());
	}
}