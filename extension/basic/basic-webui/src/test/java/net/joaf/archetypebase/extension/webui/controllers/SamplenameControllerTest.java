package net.joaf.archetypebase.extension.webui.controllers;

import net.joaf.archetypebase.extension.queries.SamplenameQuery;
import net.joaf.base.core.cqrs.CommandGateway;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

/**
 * TODO: implement
 *
 * @author Author
 * @since 1.0
 */
public class SamplenameControllerTest {

	@Mock
	private SamplenameQuery query;

	@Mock
	private CommandGateway commandGateway;

	@InjectMocks
	private SamplenameController objectUnderTest;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testList() throws Exception {
		//given
		//when
		//then
	}

	@Test
	public void testCreate() throws Exception {
		//given
		//when
		//then
	}

	@Test
	public void testEdit() throws Exception {
		//given
		//when
		//then

	}

	@Test
	public void testSaveItem() throws Exception {
		//given
		//when
		//then

	}

	@Test
	public void testTrash() throws Exception {
		//given
		//when
		//then

	}

	@Test
	public void testUndotrash() throws Exception {
		//given
		//when
		//then

	}

	@Test
	public void testCancel() throws Exception {
		//given
		//when
		//then

	}

}