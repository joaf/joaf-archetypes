package net.joaf.archetypebase.extension.webui.helpers;

import net.joaf.base.core.model.enums.EStandardAction;
import net.joaf.base.core.web.model.WebActionDetails;

/**
 * TODO: description
 *
 * @author Author
 * @since 1.0
 */
public class SamplenameActionHelper {
	public static WebActionDetails createActionDetails(EStandardAction action, String uid, String controllerContextPath) {
		WebActionDetails webActionDetails = new WebActionDetails();
		switch (action) {
		case EDIT:
			webActionDetails.setButtonStyle("btn-primary");
			webActionDetails.setActionUrl(controllerContextPath + "/edit/" + uid + ".html");
			webActionDetails.setIconName("glyphicon-edit");
			webActionDetails.setTooltip("joaf.edit");
			break;
		case TRASH:
			webActionDetails.setButtonStyle("btn-danger");
			webActionDetails.setActionUrl(controllerContextPath + "/trash/" + uid + ".html");
			webActionDetails.setIconName("glyphicon-trash");
			webActionDetails.setTooltip("joaf.trash");
			break;
		default:
			webActionDetails = null;
			break;
		}
		return webActionDetails;
	}
}
