package net.joaf.archetypebase.extension.webui.model;

import net.joaf.archetypebase.extension.model.Samplename;

import java.io.Serializable;

/**
 * TODO: description
 *
 * @author Author
 * @since 1.0
 */
public class SamplenameForm implements Serializable {

	private String uid;
	private String name;

	public SamplenameForm() {
	}

	public SamplenameForm(Samplename element) {
		this.uid = element.getUid();
	}

	public Samplename toSamplename() {
		Samplename element = new Samplename();
		element.setUid(this.getUid());
		return element;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
