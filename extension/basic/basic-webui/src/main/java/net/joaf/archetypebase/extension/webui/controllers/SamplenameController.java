package net.joaf.archetypebase.extension.webui.controllers;

import net.joaf.archetypebase.extension.commands.CancelSamplenameCommand;
import net.joaf.archetypebase.extension.commands.CreateSamplenameCommand;
import net.joaf.archetypebase.extension.commands.SaveSamplenameCommand;
import net.joaf.archetypebase.extension.commands.TrashSamplenameCommand;
import net.joaf.archetypebase.extension.commands.UndoTrashSamplenameCommand;
import net.joaf.archetypebase.extension.commands.handlers.CancelSamplenameCommandHandler;
import net.joaf.archetypebase.extension.commands.handlers.CreateSamplenameCommandHandler;
import net.joaf.archetypebase.extension.commands.handlers.SaveSamplenameCommandHandler;
import net.joaf.archetypebase.extension.commands.handlers.TrashSamplenameCommandHandler;
import net.joaf.archetypebase.extension.commands.handlers.UndoTrashSamplenameCommandHandler;
import net.joaf.archetypebase.extension.model.Samplename;
import net.joaf.archetypebase.extension.queries.SamplenameQuery;
import net.joaf.archetypebase.extension.webui.helpers.SamplenameActionHelper;
import net.joaf.archetypebase.extension.webui.model.SamplenameForm;
import net.joaf.base.core.cqrs.CommandGateway;
import net.joaf.base.core.cqrs.PageredRequest;
import net.joaf.base.core.cqrs.PageredResult;
import net.joaf.base.core.db.EObjectState;
import net.joaf.base.core.error.JoafNoSubjectContextException;
import net.joaf.base.core.session.SessionContext;
import net.joaf.base.core.subject.SubjectCardDataHelper;
import net.joaf.base.core.utils.WebFlashUtil;
import net.joaf.base.core.web.model.WrapperWDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.security.Principal;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * TODO: description
 *
 * @author Author
 * @since 1.0
 */
@Controller
@RequestMapping("/samplename")
public class SamplenameController {

	private static final Logger log = LoggerFactory.getLogger(SamplenameController.class);
	@Autowired
	private SamplenameQuery query;

	@Autowired
	private CommandGateway commandGateway;

	@RequestMapping
	public String list(HttpSession session, Model model) throws JoafNoSubjectContextException {
		SessionContext sessionContext = SessionContext.loadFromSession(session);
		SubjectCardDataHelper.checkSubjectUid(getSubjectCardDataUid(sessionContext));
		try {
			PageredRequest pageredRequest = new PageredRequest(Optional.empty(), Optional.empty(), Optional.of(EObjectState.ACCEPTED),
					getSubjectCardDataUid(sessionContext));
			PageredResult<Samplename> all = query.findAll(pageredRequest);
			all.wrap(this::createActions);
			model.addAttribute("elements", all.getElements());
		} catch (Exception e) {
			model.addAttribute("elements", Collections.emptyList());
			WebFlashUtil.addModelMessage(model, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
			log.debug("error", e);
		}
		updateModelWithPaths(model);
		return getViewitem("/list");
	}

	private void createActions(WrapperWDTO<Samplename> x) {
		x.getData().getActions().forEach(action -> x.getWebActions()
				.add(SamplenameActionHelper.createActionDetails(action, x.getData().getUid(), getControllerContextPath())));
	}

	@RequestMapping("/create")
	public String create(HttpSession session, RedirectAttributes redirectAttributes, Principal principal, Model model) throws JoafNoSubjectContextException {
		SessionContext sessionContext = SessionContext.loadFromSession(session);
		SubjectCardDataHelper.checkSubjectUid(getSubjectCardDataUid(sessionContext));
		try {
			Samplename command = (Samplename) commandGateway
					.send(new CreateSamplenameCommand(principal.getName(), getSubjectCardDataUid(sessionContext)),
							CreateSamplenameCommandHandler.class);
			return getRedirect("/edit/" + command.getUid());
		} catch (Exception e) {
			log.debug("error", e);
			WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
			return getRedirect("");
		}
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public String edit(@PathVariable("id") String id, HttpSession session, RedirectAttributes redirectAttributes, Model model)
			throws JoafNoSubjectContextException {
		SessionContext sessionContext = SessionContext.loadFromSession(session);
		SubjectCardDataHelper.checkSubjectUid(getSubjectCardDataUid(sessionContext));
		try {
			Samplename element = query.findOne(id);
			SamplenameForm command = new SamplenameForm(element);
			model.addAttribute("command", command);
			updateModelWithExtralist(model, sessionContext);
			updateModelWithPaths(model);
			return getViewitem("/edit");
		} catch (Exception e) {
			log.debug("error", e);
			WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
			return getRedirect("");
		}
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
	public String saveItem(@Valid @ModelAttribute("command") SamplenameForm command, @PathVariable("id") String id, HttpSession session,
			HttpServletRequest httpRequest, RedirectAttributes redirectAttributes, Principal principal, Model model) throws JoafNoSubjectContextException {
		SessionContext sessionContext = SessionContext.loadFromSession(session);
		SubjectCardDataHelper.checkSubjectUid(getSubjectCardDataUid(sessionContext));
		try {
			Samplename Samplename = command.toSamplename();
			commandGateway.send(new SaveSamplenameCommand(Samplename, principal.getName()), SaveSamplenameCommandHandler.class);
			WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.message.successsave");
			return getRedirect("");
		} catch (Exception e) {
			log.error("", e);
			WebFlashUtil.addModelMessage(model, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
			updateModelWithExtralist(model, sessionContext);
			updateModelWithPaths(model);
			return getViewitem("/edit");
		}
	}

	@RequestMapping("/trash/{uid}")
	public String trash(@PathVariable("uid") String uid, HttpSession session, HttpServletRequest httpRequest, RedirectAttributes redirectAttributes,
			Principal principal, Model model) throws JoafNoSubjectContextException {
		SessionContext sessionContext = SessionContext.loadFromSession(session);
		SubjectCardDataHelper.checkSubjectUid(getSubjectCardDataUid(sessionContext));
		try {
			commandGateway.send(new TrashSamplenameCommand(uid, principal.getName(), getSubjectCardDataUid(sessionContext)),
					TrashSamplenameCommandHandler.class);
			WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.message.successtrash",
					httpRequest.getContextPath() + getUrl("/undotrash/" + uid));
		} catch (Exception e) {
			log.error("", e);
			WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
		}
		return getRedirect("");
	}

	@RequestMapping("/undotrash/{id}")
	public String undotrash(@PathVariable("id") String id, HttpSession session, HttpServletRequest httpRequest, RedirectAttributes redirectAttributes,
			Principal principal, Model model) throws JoafNoSubjectContextException {
		SessionContext sessionContext = SessionContext.loadFromSession(session);
		SubjectCardDataHelper.checkSubjectUid(getSubjectCardDataUid(sessionContext));
		try {
			commandGateway.send(new UndoTrashSamplenameCommand(id, principal.getName(), getSubjectCardDataUid(sessionContext)),
					UndoTrashSamplenameCommandHandler.class);
			WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.message.successundotrash", "1");
		} catch (Exception e) {
			log.error("", e);
			WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
		}
		return getRedirect("");
	}

	@RequestMapping(value = "/cancel/{uid}", method = RequestMethod.POST)
	public String cancel(@PathVariable("uid") String uid, HttpSession session, Principal principal, Model model) throws JoafNoSubjectContextException {
		SessionContext sessionContext = SessionContext.loadFromSession(session);
		SubjectCardDataHelper.checkSubjectUid(getSubjectCardDataUid(sessionContext));
		try {
			commandGateway.send(new CancelSamplenameCommand(uid, principal.getName(), getSubjectCardDataUid(sessionContext)),
					CancelSamplenameCommandHandler.class);
		} catch (Exception e) {
			log.error("", e);
			WebFlashUtil.addModelMessage(model, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
		}
		return getRedirect("");
	}

	protected void updateModelWithExtralist(Model model, SessionContext sessionContext) {
		Map<String, String> extralistMap = new HashMap<>();
		try {
			List<String> listData = Arrays.asList("pos1", "pos2");
			for (String str : listData) {
				extralistMap.put(str, str);
			}
		} catch (Exception e) {
			log.error("Error create list", e);
		}
		model.addAttribute("extralist", extralistMap);
	}

	protected String getSubjectCardDataUid(SessionContext sessionContext) {
		return sessionContext.getSubjectCardDataId();
	}

	protected void updateModelWithPaths(Model model) {
		model.addAttribute("backPath", getBackPath());
		model.addAttribute("controllerContextPath", getControllerContextPath());
	}

	protected String getBackPath() {
		return "/";
	}

	protected String getControllerContextPath() {
		return "/samplename/template";
	}

	protected String getViewitem(String name) {
		return "/freemarker/samplename/basic" + name;
	}

	protected String getRedirect(String name) {
		return "redirect:" + getUrl(name);
	}

	protected String getUrl(String name) {
		return "/samplename" + name + ".html";
	}
}
