package net.joaf.archetypebase.extension.repository;

import net.joaf.archetypebase.extension.model.Samplename;
import net.joaf.base.core.db.CrudRepository;
import net.joaf.base.core.db.JoafRepository;
import net.joaf.base.core.db.NoSqlRepository;

/**
 * TODO: description
 *
 * @author Author
 * @since 1.0
 */
public interface SamplenameRepository extends NoSqlRepository, JoafRepository, CrudRepository<Samplename> {
}
