package net.joaf.archetypebase.extension.repository;

import net.joaf.base.core.db.CrudRepository;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertTrue;

/**
 * @author Author
 * @since 1.0
 */
public class SamplenameRepositoryTest {

    @Test
    public void interfaceTest() {
        //given
        //when
        Class<SamplenameRepository> samplenameRepositoryClass = SamplenameRepository.class;
        //then
        assertTrue(samplenameRepositoryClass.isInterface());
        assertTrue(Arrays.asList(samplenameRepositoryClass.getInterfaces()).contains(CrudRepository.class));
    }

}
