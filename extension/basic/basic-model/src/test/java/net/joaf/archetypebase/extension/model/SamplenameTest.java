package net.joaf.archetypebase.extension.model;

import org.junit.Test;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * TODO: description
 *
 * @author Author
 * @since 1.0
 */
public class SamplenameTest {

    @Test
    public void modelTest() {
        //given
        //when
        Samplename samplename = new Samplename();
        //then
        assertTrue(samplename.getActions().isEmpty());
        assertNull(samplename.getSubjectUid());
    }
}
