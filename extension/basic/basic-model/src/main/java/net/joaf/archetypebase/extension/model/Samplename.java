package net.joaf.archetypebase.extension.model;

import net.joaf.base.core.db.entity.JoafSuidEntity;
import net.joaf.base.core.model.enums.EStandardAction;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * TODO: description
 *
 * @author Author
 * @since 1.0
 */
public class Samplename extends JoafSuidEntity implements Serializable {

    private String name;

    private List<EStandardAction> actions = new ArrayList<EStandardAction>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<EStandardAction> getActions() {
        return actions;
    }
}
