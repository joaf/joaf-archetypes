#!/bin/bash
rm -rf target
mvn archetype:create-from-project -Darchetype.properties=../basic.properties
for fil in $(find target -name *Samplename\*.\*); do
	renamed=$(echo ${fil} | sed 's/Samplename/__classPrefix__/')
	mv $fil $renamed
done
for fil in $(find target -name *samplename\*.\*); do
    renamed=$(echo ${fil} | sed 's/samplename/__baseName__/')
    mv $fil $renamed
done
for fil in $(find target -name .idea); do
    rm -rf ${fil}
done
for fil in $(find target -name run.sh); do
    rm -rf ${fil}
done

mvn -f target/generated-sources/archetype/pom.xml install

